#!/usr/bin/env bash

## Pull Docker images listed  in an env file
## Read parameters between ### DOCKER IMAGES ### and ###
## Docker image must have variable name with *_IMAGE
## Docker image tag must have variable name with *_IMAGE_TAG
## Docker image and docker image tag must be in order
## Ex: 
##     AF_IMAGE=ebsproject/af
##     AF_IMAGE_TAG=latest
#
##     B4R_IMAGE=ebsproject/b4r
##     B4R_IMAGE_TAG=latest


file=$1

DOCKER_IMAGE_STR="_IMAGE"
DOCKER_IMAGE_TAG_STR="_IMAGE_TAG"

DOCKER_IMAGE=""
DOCKER_IMAGE_TAG=""

while read -r line; do
    ## Skip lines
    [[ "$line" == "### DOCKER IMAGES ###" ]] && [ -z "$line" ] && [[ "$line" =~ ^#.*$ ]] && continue

    ## Stop reading the file
    [[ "$line" = ^#.*$ ]] && exit

    ## Check if lines has *_IMAGE
    if [ -z "${line##*$DOCKER_IMAGE_STR*}" ] ;then
        
        ## Check if lines has *_IMAGE_TAG
        if [ -z "${line##*$DOCKER_IMAGE_TAG_STR*}" ] ;then
            ## Set DOCKER_IMAGE_TAG
            DOCKER_IMAGE_TAG=$(echo $line | cut -d "=" -f2)
        else
            ## Set DOCKER_IMAGE
            DOCKER_IMAGE=$(echo $line | cut -d "=" -f2)
        fi
    fi

    ## Check if DOCKER_IMAGE & DOCKER_IMAGE_TAG is updated
    if [[ ${DOCKER_IMAGE} != "" ]] && [[ ${DOCKER_IMAGE_TAG} != "" ]] ;then
        ## Pull the docker images
        {
            docker pull ${DOCKER_IMAGE}:${DOCKER_IMAGE_TAG}
        } || {
            exit 1
        }
        ## Set the variables to empty
        DOCKER_IMAGE=""
        DOCKER_IMAGE_TAG=""
    fi
    
done < "$file"