#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
### Set ENV

set -e # Abort script at first error
set -u # Attempt to use undefined variable outputs error message

#-----------------------------------------------------------------------------#
### Set Colors
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
BLUE=`tput setaf 4`
RESET=`tput sgr0`

#-----------------------------------------------------------------------------#
# Statement for start

echo "$BLUE ---------------------------------------------------- $RESET"
echo "$BLUE Starting the deployment of Vault.. $RESET"
echo;

#-----------------------------------------------------------------------------#

## Retrieve current directory
echo "$BLUE ---------------------------------------------------- $RESET"
echo "$BLUE [1/3] Retrieving current directory.. $RESET"
echo;

pwd=`pwd`
export DATA_DIR=$pwd
echo $DATA_DIR

## Create directories for Vault
set -x

mkdir -p $DATA_DIR/volume/vault/logs
mkdir -p $DATA_DIR/volume/vault/file
mkdir -p $DATA_DIR/volume/vault/policies


set +x

export VAULT_LOGS="$(cd $DATA_DIR/volume/vault/logs; pwd)"
export VAULT_FILE="$(cd $DATA_DIR/volume/vault/file; pwd)"
export VAULT_POLICIES="$(cd $DATA_DIR/volume/vault/policies; pwd)"

echo $VAULT_LOGS
echo $VAULT_FILE
echo $VAULT_POLICIES

echo;
echo "$GREEN ☑  DONE. $RESET"
echo;

#-----------------------------------------------------------------------------#

## Pull Docker images
echo "$BLUE ---------------------------------------------------- $RESET"
echo "$BLUE [2/3] Loading Docker images.. $RESET"
echo;

{
    bash utility/pull_images.sh env && {
        echo;
        echo "$GREEN ☑  DONE. $RESET"
        echo;
    }
} || {
    echo;
    echo "$RED ☒ Error in loading Docker images. $RESET"
    exit 1
}
#-----------------------------------------------------------------------------#

## Load environment variables containing parameters for Vault stack and deploy

echo "$BLUE ---------------------------------------------------- $RESET"
echo "$BLUE [3/3] Deploying stack... $RESET"
echo;

{
    set -x
    env $(cat env | grep ^[A-Z] | xargs) \
        docker stack deploy --with-registry-auth -c docker-compose.yml vaultstack && {
            set +x
            echo;
            echo "$GREEN ☑  DONE. $RESET"
            echo;
            echo "$GREEN ---------------------------------------------------- $RESET"
            echo "$GREEN ☑  Deployment is complete. $RESET"
            echo;
        }
} || {
    set +x
    echo;
    echo "$RED ☒  Error in deploying stack. Please run the script again. $RESET"
    echo;
}